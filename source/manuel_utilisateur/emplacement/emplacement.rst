.. _emplacement:

################
Les emplacements
################

Il existe six types d'emplacements différents dans l'applicatif :

* :ref:`concession`,
* :ref:`colombarium`,
* :ref:`enfeu`,
* :ref:`ossuaire`,
* :ref:`depositoire`,
* :ref:`terraincommunal`.


On accède à ces différents emplacements depuis le menu
(:menuselection:`Emplacements`).

.. image:: a_emplacement-menu-rubrik-emplacement.png

Seul le type d'emplacement concession est décrit ici car c'est le plus
exhaustif. Les autres types possèdent moins d'éléments.

.. _concession:

La concession
=============

Cet élément est accessible via (:menuselection:`Emplacements --> Concession`).

La concession est une place dans un cimetière.
http://fr.wiktionary.org/wiki/concession

.. image:: a_emplacement-emplacement_concession-listing.png

Le formulaire est identique en mode ajout et modification.

.. image:: a_emplacement-emplacement_concession-formulaire-modification.png

Les informations à saisir sont :

* numéro dans la voie
* voie
* famille


.. _colombarium:

Le colombarium
==============

Cet élément est accessible via (:menuselection:`Emplacements --> Colombarium`).

Aussi appelé columbarium, le colombarium est un édifice destiné à recevoir des
urnes mortuaires dans les cimetières où l’on pratique l’incinération.
http://fr.wiktionary.org/wiki/columbarium


.. _enfeu:

L'enfeu
=======

Cet élément est accessible via (:menuselection:`Emplacements --> Enfeu`).

L'enfeu est une niche dans un édifice religieux abritant un tombeau, un
sarcophage ou une scène funéraire.
http://fr.wiktionary.org/wiki/enfeu


.. _ossuaire:

L'ossuaire
==========

Cet élément est accessible via (:menuselection:`Emplacements --> Ossuaire`).

L'ossuaire est un endroit couvert où l’on met des ossements humains.
http://fr.wiktionary.org/wiki/ossuaire


.. _depositoire:

Le dépositoire
==============

Cet élément est accessible via (:menuselection:`Emplacements --> Dépositoire`).

Le dépositoire est le nom donné, dans quelques localités, au lieu où l’on dépose
les corps des morts, avant de les enterrer, et jusqu’à ce que la décomposition
putride commence à se manifester.
http://fr.wiktionary.org/wiki/d%C3%A9positoire



.. _terraincommunal:

Le terrain communal
===================

Cet élément est accessible via
(:menuselection:`Emplacements --> Terrain Communal`).

Appelé aussi "terrain commun", le terrain communal est ...

Deux valeurs sont présentes par défaut au chargement du formulaire :

- superficie_terraincommunal : valeur par défaut 2 (:ref:`paramétrage général <superficie_terraincommunal>`)
- duree_terraincommunal : valeur par défaut 5 (:ref:`paramétrage général <duree_terraincommunal>`)



################################
La localisation de l'emplacement
################################

Option Plan
===========

La localisation d'un emplacement sur un plan possède deux modes :

* le positionnement de l'emplacement : depuis le formulaire de modification de
  l'emplacement, il est possible de cliquer sur l'action "Positionner l'élément"
  pour obtenir un écran de positionnement sur le plan.

  .. image:: a_emplacement-emplacement_concession-formulaire-modification-bloc-localisation-plan.png
  
  Au clic sur la punaise, une fenêtre s'ouvre avec le plan et l'emplacement
  positionné en fonction des coordonnées X et Y présentes dans les champs. Il
  suffit de déplacer le point bleu avec la souris pour le positionner à
  l'endroit souhaité. Un double clic sur ce point bleu fermera le plan et
  positionnera les nouvelles coordonnées dans le formulaire. Il faut enregistrer
  le formulaire de l'emplacement pour que les modifications soient prises en
  compte.

* la visualisation de l'emplacement : depuis le tableau ou depuis le formulaire
   il est possible de visualiser ou mettre à jour le positionnement de l'emplacement sur la carte.

  .. image:: a_emplacement-emplacement_concession-fiche-visualisation-bloc-localisation-plan.png


Option SIG
==========

SIG interne
-----------

.. image:: a_emplacement-emplacement_concession-fiche-visualisation-bloc-localisation-sig_interne.png

Il est possible de géolocaliser l'emplacement sur une carte.

Nous proposons de géolocaliser la concession 2 dans le cimetière de Moulès (exemple dans data/pgsql/init_data.sql)
Par défaut, il nous est présenté un polygone qui est l'espace le plus précis concernant la localisation de la concession :
soit la voie si il est géolocalisée, soit la zone, soit enfin le cimetière.

Dans notre cas, la voie de la concession 2 est géolocalisée dans la table voie :

.. image:: geo_localisation1.png

Nous allons d'abord positionner un point :

Nous cliquons dans le cartouche sig (fenêtre de droite) sur outil (en haut) puis sur "éditer", nous choisissons "dessiner un point", nous cliquons sur la carte et un point bleu est visible. Nous appuyons sur "selectionner une géométrie", nous sélectionnons le point bleu en cliquant dessus, il devient rouge. 

.. image:: geo_localisation2.png

En appuyant sur "Enregistrer", une fenêtre s'ouvre nous proposant de valider la géométrie point.

.. image:: geo_localisation3.png

En validant, un point et une bulle apparaisse sur l'écran :

.. image:: geo_localisation4.png


Nous proposons maintenant de créer une géométrie polygone associé à la concession 2.
Nous retournons dans l'onglet "outils" du cartouche sig (a droite) et nous cliquons sur "editer"
Nous choisissons la géométrie polygone (emplacement) et nous choisissons le pannier "plan"
Un plan des emplacements apparait sur la carte

.. image:: geo_localisation5.png

Nous choisissons l emplacement sous notre point qui devient plus foncé

.. image:: geo_localisation6.png

Nous appuyons sur "récupération pannier", le polygone devient bleu

.. image:: geo_localisation7.png

Nous appuyons sur "selection géométrie" et nous cliquons sur le polygone : il devient rouge

.. image:: geo_localisation8.png

En appuyant sur enregistrer, il est proposé une fenêtre de validation.

.. image:: geo_localisation9.png

Notre concession a un point d'emplacement et un polygone d'emplacement.

Le polygone peut aussi être dessiné s'il n'y a pas d'emplacement déjà dessiné.

Une concession peut avoir plusieurs polygones.

###########################################
Affichage du plan en coupe de l'emplacement
###########################################

Pour accéder au plan en coupe de l'emplacement il faut tout d'abord s'assurer du bon paramétrage du :ref:`type de sépulture <sepulture_type>` de l'emplacement.
Lorsque le type de sépulture est bien paramétré, l'action permettant d'accéder au plan en coupe est disponible dans le porlet d'action contextuel.

.. image:: a_emplacement-emplacement_concession-portlet-action-plan-en-coupe.png

Le plan en coupe est composé d'une colonne contenant toutes les cartes des défunts qui ne sont pas encore été positionnées dans le plan en coupe et une grille contenant le nombre de ligne et de colonne paramétré dans le type de sépulture. Les défunts peuvent être positionnés dans cette grille, pour cela il faut effectuer l'action glisser-déposer dans la case voulu. Il est possible d'avoir plusieurs défunts dans la même case.

.. image:: a_emplacement-emplacement_concession-plan-en-coupe.png

Il est possible de consulter, modifier ou supprimer les informations du défunt directement à partir du lien *Voir plus* présent sur chaque carte de défunt qui ouvre un overlay sur la même page.

.. image:: a_emplacement-emplacement_concession-plan-en-coupe-defunt-overlay.png

Lorsqu'un vide sanitaire est présent sur l'emplacement, il est représenté sur le plan en coupe comme une ligne supplémentaire ajouté automatiquement au nombre de ligne paramétré dans le type de sépulture.

.. image:: a_emplacement-emplacement_concession-plan-en-coupe-vide-sanitaire.png

Si il n'y a pas de défunt dans une case de la grille, il est possible de verrouiller cette case en cliquant sur le bouton de verrouillage représenté par un cadenas. Les défunts ne peuvent pas être placer dans une case verrouillée.