.. _genealogie:

#############
La généalogie
#############


Il est proposé de décrire dans ce paragraphe la saisie des généalogies
dans l'onglet "Généalogie" de l'emplacement.
Toutes les généalogies liées à l'emplacement sont listées dans cet onglet.

.. image:: a_emplacement-emplacement_concession-onglet-genealogie-listing.png

Avant de saisir une généalogie il faut s'assurer du bon paramétrage des 
:ref:`liens de parenté <lien_parente>` dans l'application.
Il faut aussi que l'emplacement contienne des défunts et/ou des contacts.


Saisir une généalogie
#####################

Il y a trois champs à saisir obligatoirement pour ajouter une généalogie.
Le formulaire est identique en mode ajout et modification.

.. image:: a_emplacement-emplacement_concession-onglet-genealogie-formulaire-ajout.png

Les informations à saisir sont :

- La première personne à lier. Le champ est une liste déroulante regroupant les contacts et les défunts.
- Le lien de parenté. À choisir en fonction de ce qui a été paramétré dans l'applicatif.
- La deuxième personne à lier. Idem que pour la première personne, le champ est une liste déroulante regroupant les contacts et les défunts.

Lorsque le lien a été ajouté, on peut le retrouver dans le champ **Généalogie** présent sur les fiche synthèse de défunt et contact.
Ce champ contient la liste des généalogies pour la personne sélectionné que ce soit pour les liaisons et les liaisons inverse.

Lorsqu'un défunt est lié à un contact et que la liaison est de type "même personne", alors toutes les liaisons de l'un sont affichés sur l'autre. 