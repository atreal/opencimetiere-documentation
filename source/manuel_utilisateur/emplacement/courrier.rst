.. _courrier:

#############
Les courriers
#############

Il est proposé de décrire dans ce paragraphe la saisie de courrier dans l'onglet
"courrier" de l'emplacement.

On accède à cet élément depuis l'onglet "Courrier" de l'emplacement.
Tous les courriers liés à l'emplacement sont listés dans cet onglet.

.. image:: a_emplacement-emplacement_concession-onglet-courrier-listing.png


Le formulaire est identique en mode ajout et modification.

.. image:: a_emplacement-emplacement_concession-onglet-courrier-formulaire-ajout.png


Les informations à saisir sont :

- le destinataire (concessionnaire, ayant droit ou autre). Concernant le nom du destinataire, on affiche soit le nom d'usage si le destinataire en possède un, sinon le nom de naissance.
- la date d envoi (par défaut la date du jour)
- la lettre type (le courrier type à envoyer)
- le complément : texte inséré dans la lettre type (suivant le paramétrage)

