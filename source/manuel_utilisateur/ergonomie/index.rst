.. _ergonomie:

#########
Ergonomie
#########

.. _ergonomie_generale:

******************
Ergonomie générale
******************

L'application, sur la grande majorité des écrans, conserve ses composants
disposés exactement au même endroit. Nous allons décrire ici le fonctionnement
et l'objectif de chacun de ces composants. Cette structuration de l'application
permet donc à l’utilisateur de toujours trouver les outils au même endroit
et de se repérer rapidement.

.. figure:: a_ergonomie_generale_detail.png

    Ergonomie générale

.. note::

    Les actions et affichages de l'application diffèrent en fonction du profil
    de l'utilisateur. Il se peut donc que dans les paragraphes qui suivent
    des actions soient décrites et n'apparaissent pas sur votre interface
    ou inversement que des actions ne soient pas décrites mais apparaissent sur
    votre interface.

=======
Le logo
=======

C'est le logo de l'application, il vous permet en un seul clic de revenir
rapidement au tableau de bord.

.. figure:: a_ergonomie_logo.png

    Logo

========================
Les actions personnelles
========================

Cet élément affiche plusieurs informations importantes.

La première information est l'identifiant de l'utilisateur actuellement
connecté ce qui permet de savoir à tout moment si nous sommes bien connectés
et avec quel utilisateur. Ensuite est noté le nom de la collectivité sur
laquelle nous sommes en train de travailler. En mode multi, une action est
disponible sur cette information pour permettre de changer de collectivité.
Ensuite la liste sur laquelle nous sommes en train de travailler, une action
est disponible sur cette information pour permettre de changer de liste.
Enfin l'action pour permettre de changer de mot de passe et pour se déconnecter
sont disponibles en permanence.

.. figure:: a_ergonomie_actions_personnelles.png

    Actions personnelles

==============
Les raccourcis
==============

Cet élément permet d'afficher des raccourcis vers des écrans auxquels nous
avons besoin d'accéder très souvent. Par exemple, ici nous avons un 
raccourci direct vers le tableau de bord.

.. figure:: a_ergonomie_raccourcis.png

    Raccourcis

=======
Le menu
=======

Cet élément permet de classer les différents écrans de l'application en
rubriques. En cliquant sur l'entête de rubrique, nous accédons à la liste des
écrans auxquels nous avons accès dans cette rubrique.

Le nombre de rubriques disponibles dans le menu peut varier en fonction du
profil des utilisateurs. Un utilisateur ayant le profil Consultation n'aura
probablement pas accès aux six rubriques présentes sur cette capture.

.. figure:: a_ergonomie_menu.png

    Menu


====================
Les actions globales
====================

Cet élément permet d'afficher en permanence le numéro de version du logiciel.
Ensuite les différentes actions sont des liens vers le site officiel du
logiciel ou vers la documentation.

.. figure:: a_ergonomie_actions_globales.png

    Actions globales

*************************
Ergonomie des formulaires
*************************

De manière générale, il y a une règle simple dans les applicatifs openMairie :
on accède d'abord à un listing d'éléments puis depuis ce listing on peut
ajouter un nouvel élément ou modifier un élément existant en accédant au
formulaire dédié à cet élément.

============
Les listings
============

Un listing est un tableau qui liste des éléments récapitulant des informations
permettant d'identifier un élément parmi les autres.

.. image:: a_ergonomie-exemple-listing.png


Les actions
===========

En haut à gauche
----------------

* Ajouter : cette action représentée par un plus permet d'accéder au formulaire
  de création d'un élément.
  
  |icone-ajouter|

* Autre : il peut y avoir d'autres actions positionnées ici qui représentent
  des actions que l'on peut faire sur un lot d'éléments par exemple.


A gauche devant chaque élément
------------------------------

* Visualiser : cette action permet d'accéder au formulaire de visualisation
  d'un élément.
  
  |icone-visualiser|

* Autre : il peut y avoir d'autres actions positionnées ici qui permettent
  d'effectuer des actions rapides sans avoir besoin d'accéder au formulaire
  de l'élément en visualisation puis de cliquer sur une action parmi les
  actions contextuelles de l'élément.


Sur l'élément
-------------

* Visualiser : cette action permet d'accéder au formulaire de visualisation
  d'un élément.


Divers
------

* Afficher les éléments expirés : sur les élements qui possèdent une date de
  validité, par défaut les éléments qui sont dans le passé n'apparaissent pas,
  il est nécessaire de cliquer sur cette action pour les faire apparaître.
  L'action se situe au dessus du tableau.

* Imprimer le listing en PDF : sur les éléments pour lesquels l'édition existe
  une action représentée par une imprimante permet de télécharger un pdf qui
  reflète le contenu du listing sans aucun filtre de recherche. L'action se
  situe au dessus du tableau.
  
  |icone-edition-pdf|

===============
Les formulaires
===============

Un formulaire dans cet applicatif peut soit être de visualisation soit d'action
(ajout, modification, suppression, ...). La différence est que le premier
contient un portlet d'actions contextuelles et que le deuxième contient un
bouton qui permet de valider le formulaire en question.

Voici l'exemple d'un formulaire en mode visualisation.

.. image:: a_ergonomie-exemple-fiche-visualisation.png
    

Voici l'exemple d'un formulaire en mode modification.

.. image:: a_ergonomie-exemple-formulaire-modification.png


Les actions
===========

Le portlet d'actions contextuelles se trouve sur le formulaire d'un élément
en mode visualisation. Il contient normalement toutes les actions possibles sur
cet élément par l'utilisateur. Il est situé en haut à droite du formulaire.


Actions de formulaires
----------------------

* Modifier : cette action permet de transformer le mode visualisation de
  l'élément en mode modification. Une fois le formulaire de modification validé
  alors un bouton retour nous permet de revenir au formulaire en mode
  visualisation.

* Supprimer : cette action permet de transformer le mode visualisation de
  l'élément en mode suppression. Une fois le formulaire de suppression validé
  alors un bouton retour nous permet de revenir au listing des éléments.


Autres actions
--------------

* Le portlet d'acctions contextuelles peut contenir toutes les actions
  disponibles sur l'élément. Par exemple : il peut y avoir une action qui
  permet d'imprimer une édition PDF de l'élément, une autre action permettant
  de changer une valeur spécifique de l'élément comme "Marquer comme lu", ...


Les onglets
===========

Sur le formulaire d'un élément, il peut apparaître plusieurs onglets qui
correspondent à des éléments liés à l'élément en cours. Un onglet présente un
listing de ces éléments liés avec des actions qui permettent également d'accéder
à des formulaires sur ces éléments liés.

Par exemple sur l'image suivante, on peut voir sur le "profil" un onglet
"tableau de bord" qui liste les tableaux de bord liés au profil utilisateur.

.. image:: a_ergonomie-exemple-onglet-exemple-listing.png


.. |icone-edition-pdf| image:: a_ergonomie-icone-pdf-listing.png
.. |icone-ajouter| image:: a_ergonomie-icone-ajouter.png
.. |icone-visualiser| image:: a_ergonomie-icone-visualiser.png

.. _connexion_deconnexion:

*************************************
Connexion, déconnexion et permissions
*************************************

=========
Connexion
=========

.. note::

   Pour réaliser cette étape, votre administrateur doit vous fournir une
   adresse Web pour accéder à l'application, un identifiant utilisateur ainsi
   qu'un mot de passe. Ces éléments auront été préalablement configuré dans
   le logiciel.


Navigateur Web
==============

L'application est accessible via un navigateur Web, pour y accéder il faut
saisir l'adresse Web fournie par votre administrateur dans la barre d'adresse.

.. figure:: m_connexion_navigateur.png

    Saisie d'adresse dans un navigateur Web

.. note::

    Ce logiciel est développé principalement sous le navigateur Mozilla Firefox,
    il est donc conseillé d'utiliser ce navigateur pour une efficacité optimale.


Saisie des informations de connexion
====================================

Cet écran de connexion est composé de deux zones de texte et d'un bouton.

.. figure:: a_connexion_formulaire.png

    Formulaire de connexion

La figure 2 présente l'écran d'identification, il faut saisir son identifiant et
son mot de passe puis cliquer sur le bouton « Se connecter ».

.. note::

    L'identifiant et le mot de passe doivent être saisis en respectant la
    casse, c'est-à-dire les minuscules et majuscules.


Connexion échouée
-----------------

Si les identifiants saisis sont incorrects, un message d'erreur apparaît et il
faut ressaisir les informations de connexion.

.. figure:: a_connexion_message_erreur.png

    Message de connexion échouée


Connexion réussie
-----------------

Si les identifiants sont corrects, vous êtes redirigé vers la page demandée sur
laquelle le message suivant doit d'afficher.

.. figure:: a_connexion_message_ok.png

    Message de connexion réussie


===========
Déconnexion
===========

Pour une question de sécurité évidente, il est important de se déconnecter de
l'application pour qu'aucun autre utilisateur ne puisse pas accéder au logiciel
via votre compte utilisateur.

L'action "Déconnexion" est disponible à tout moment dans les actions
personnelles en haut à droite de l'écran.

.. figure:: a_deconnexion_action.png

   Action "Déconnexion" dans la barre d'actions personnelles

Une fois déconnecté, c'est le formulaire de connexion qui s'affiche avec un
message expliquant la réussite de la déconnexion.

.. figure:: a_deconnexion_message_ok.png

    Message de déconnexion réussie


======================================
Les droits et profils des utilisateurs
======================================

Les droits et profils des utilisateurs permettent de limiter l'accès aux
informations et aux actions uniquement aux personnes autorisées. Chaque
utilisateur est associé à un profil. Le profil correspond à un ensemble
de permissions de l'utilisateur, par défaut il existe cinq profils :

#. Consultation,

#. Utilisateur limité,

#. Utilisateur,

#. Super utilisateur,

#. Administrateur.

Chaque page de l'application est associée à un profil. Pour chaque accès à une
page, l'application vérifie si l'utilisateur a un profil supérieur ou égal au
profil de la page consultée, si c'est le cas l'utilisateur à donc le droit
d'accéder aux informations.



.. _tableau_de_bord:

***************
Tableau de bord
***************

Le tableau de bord est composé de plusieurs blocs d'informations appelés widget qui permettent à l'utilisateur de visualiser rapidement des informations transverses.

.. image:: a_tableau-de-bord-exemple.png

La disposition des widgets est propre à chaque profil et peut être modifiée très facilement par l'administrateur. Il est donc possible pour les services de modifier la disposition (suppression de widget / déplacement de widget).


=======
Widgets
=======

.. _widget_recherche_globale:

Le widget Recherche globale
===========================

Cet widget a pour objectif de permettre d'effecteur une recherche directement
depuis le tableau de bord. Les critères de recherche sont identiques à ceux de
l'écran :ref:`recherche_globale`. C'est d'ailleurs sur cet écran que les
résultats vont s'affichés lors de la validation du formulaire du widget.

.. image:: a_widget_recherche_globale.png


.. _widget_localisation:

Le widget Localisation
======================

Ce widget a pour objectif de donner des accès rapides aux fonctions de
localisation en fonction de l'option de localisation paramétrée. Le paramètre
"option_localisation" (:ref:`paramétrage général <option_localisation>`) change
le contenu de ce widget.


Option Plan
-----------

Si aucun plan n'est configuré alors le widget affiche un message prévenant
l'utilisateur qu'aucun plan n'est configuré.

Si au moins un plan est configuré alors ce widget affiche la liste des plans
avec un lien vers l'action "Visualiser un plan" (Voir :ref:`plans`).


.. image:: a_widget_localisation_plan.png


Option SIG
----------

- SIG interne

Si il y a au moins un cimetiere saisi avec sa géométrie, ce widget affiche le plan du cimetiere avec l'ensemble des emplacements
accessibles



.. _widget_supervision:

Le widget Supervision
=====================

L'objectif de ce widget est de présenter le nombre d'emplacements par type et
par cimetière dans l'applicatif.

.. image:: a_widget_supervision.png


.. _widget_concession_a_terme:

Le widget Concession à terme
============================

Ce widget a pour objectif d'afficher les quinze concessions à terme et de donner
un accès direct à l'écran :ref:`concession_a_terme` (s'y référer pour connaître
le filtre appliqué aux résultats de ce widget). Chaque lien présent dans ce widget
redirige vers cet écran.


.. image:: a_widget_concession_a_terme.png

Le widget Contrats à valider
============================

Ce widget a pour objectif d'afficher quinze contrat à valider et de donner
un accès direct à l'écran :ref:`recherche_contrat` avec la valeur de valide à 'Non' dans la recherche avancée. 

.. image:: a_widget_contrat_a_valider.png

