.. _geolocalisation:

##################
La géolocalisation
##################

La géolocalisation est un procédé permettant de positionner un objet (une
personne, un bâtiment, ...) sur un plan ou une carte à l'aide de ses coordonnées
géographiques.

Dans cet applicatif, il existe trois possibilités pour géolocaliser les
emplacements. C'est le paramètre "option_localisation"
(:ref:`paramétrage général <option_localisation>`) qui doit être positionné
sur une des options disponibles :

- :ref:`plans`,
- :ref:`option_sig_interne`,



.. _plans:

Option Plan
===========

Un plan correspond à un croquis d'un cimetière ou d'une partie d'un cimetière.
Un emplacement se localise sur un plan grâce à un point.

.. note::

    Cette option est conservée pour des raisons de compatibilité avec
    l'ancienne version mais il est important de signaler qu'elle ne
    constitue pas un véritable système de géolocalisation même si elle apparait
    comme simple et pratique.




Cet élément est accessible via 
(:menuselection:`Paramétrage --> Localisation --> Plans`).

.. image:: a_administration-plans-listing.png


Saisir un plan
--------------

Il est possible de créer ou modifier un plan dans le formulaire ci-dessous :

.. image:: a_administration-plans-formulaire-ajout.png

Les informations à saisir sont :

- le fichier à télécharger
- le libéllé du plan


Visualiser un plan
------------------

|icone-localiser|

Cette action permet de visualiser le plan avec tous les emplacements
positionnés sur ce dernier. Au survol d'un emplacement, le nom de famille de
l'emplacement apparaît et en cliquant sur le point on accède à la fiche de
l'emplacement en visualisation.

Code couleur des points :

* Un point jaune représente un emplacement temporaire.
* Un point vert représente un emplacement libre.
* Un point rouge représente un emplacement à perpétuité.


.. image:: a_administration-plans-visualisation-globale-exemple-1.png

.. image:: a_administration-plans-visualisation-globale-exemple-2.png


.. _option_sig:

Option SIG
==========

.. _option_sig_interne:

SIG interne
-----------

Dans la rubrique administration du menu, il est possible de paramétrer les cartes (om_sig_map) et de paramétrer les flux (om_sig_flux) si l'option sig_interne est activée. (voir le guide du développeur openMairie : https://docs.openmairie.org/?project=framework&version=4.9&path=usage/administration/index.html#le-sig)

Des exemples de paramétrage SIG interne sont disponibles dans les scripts `tests/data/pgsql/init_parametrage_sig_*.sql` de l'archive téléchargée.

.. |icone-localiser| image:: a_administration-icone-geolocaliser.png



