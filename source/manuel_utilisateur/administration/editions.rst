.. _editions:

############
Les éditions
############


Les états et lettres types
==========================

(:menuselection:`Paramétrage --> Éditions --> État`) 
(:menuselection:`Paramétrage --> Éditions --> Lettre Type`)

Paramétrage des informations générales de l'édition
***************************************************

.. image:: a_parametrage_etat_lettretype_edition.png

Les informations d'édition à saisir sont :

    * **id** : identifiant de l’état/lettre type.
    * **libellé** : libellé affiché dans l’application lors de la sélection d’une édition.
    * **actif** : permet de définir si l’édition est active ou non.


.. note::
	
	Les champs **id** et **libellé** sont obligatoires, les **id** actif sont uniques.


Les champs de paramètres généraux de l’édition à saisir sont :

    * **orientation** : orientation de l'édition (portrait/paysage).
	* **format** : format de l'édition (A4/A3).
	* **logo** : sélection du logo depuis la table des logos configurés.
	* **logo haut/gauche** : position du coin haut/gauche du logo par rapport au coin
	  haut/gauche de l'édition.
	* **Marge gauche** : marge gauche de l'édition
	* **Marge haut** : marge haute de l'édition
	* **Marge droite** : marge droite de l'édition
	* **Marge bas** : marge basse de l'édition

Paramétrage du titre de l'édition.
**********************************

.. image:: a_parametrage_etat_lettretype_titre.png


* **titre** : éditeur riche permettant une mise en page complexe.

Paramètres du titre de l'édition.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Positionnement :

* **titre gauche** : positionnement du titre par rapport à la marge gauche de l'édition.
* **titre haut** : positionnement du titre par rapport à la marge haute de l'édition.
* **largeur de titre** : taille de la largeur du titre.
* **hauteur** : hauteur minimum du titre.

Bordure :

* **bordure** : Affichage ou non d'une bordure.

Paramétrage du corps de l'édition.
**********************************

.. image:: a_parametrage_etat_lettretype_corps.png

* **corps** : éditeur riche permettant une mise en page complexe.

.. note::

    Il est possible d'ajouter les sous-états paramétrés via le menu **Insérer->
    Sous-états**, un sous-état de chaque type peut être affiché.

Paramétrage des champs de fusions de l'édition
**********************************************

En fonction de la requête sélectionnée, on peut utiliser les champs de fusion de la table choisie et de ses tables liées.

.. image:: a_parametrage_etat_lettretype_sql.png

* **Requête** : sélection d'un jeu de champs de fusion.

