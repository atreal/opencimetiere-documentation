.. _parametrage:

############################
Administration & Paramétrage
############################

Une section dédiée à l'administration et au pamétrage permet de configurer l'application : saisir les données des tables de références, gérer les utilisateurs, paramétrer les éditions PDF, ... Pour y accéder, un lien est disponible dans les actions 'raccourcis'.

.. figure:: a_administration_parametrage-action.png

    Raccourci 'Administration & Paramétrage'

Un menu permet de lister tous les écrans d'administration et paramétrage disponibles pour le profil de l'utilisateur connecté. Une recherche permet de filtrer les éléments disponibles dans ce menu.

.. figure:: a_administration_parametrage-menu.png

    Menu 'Administration & Paramétrage'

Dans ce paragraphe il est décrit les principaux éléments pour le manuel utilisateur, si un élément n'est pas décrit ici, il faut se référer au guide du développeur du framework openMairie :
https://docs.openmairie.org/?project=framework&version=4.10&format=html&path=usage/administration


.. toctree::

    tables_de_reference.rst
    tables_de_localisation.rst
    geolocalisation.rst
    parametres_generaux.rst
    editions.rst
    module_import.rst
