.. opencimetiere documentation master file.

===============================
openCimetière 4.2 documentation
===============================

.. note::

    Cette création est mise à disposition selon le Contrat Paternité-Partage des
    Conditions Initiales à l'Identique 2.0 France disponible en ligne
    http://creativecommons.org/licenses/by-sa/2.0/fr/ ou par courrier postal à
    Creative Commons, 171 Second Street, Suite 300, San Francisco,
    California 94105, USA.


Créé dans un groupe de l'ADULLACT en 2005, openCimetière se place dans un
contexte de gestion difficile, dans ce secteur d'activité considéré comme peu
stratégique par les décideurs et pourtant combien important pour le service
public de proximité. Logiciel libre opérationnel depuis des années,
openCimetière a été en septembre 2006 élu "projet du mois" sur la forge. 
http://www.openmairie.org/catalogue/opencimetiere

openCimetière est un outil souple et adaptable à toute collectivité quelle que 
soit sa taille. Les principales fonctions de l'application sont les suivantes :

* la gestion de la place (défunt) dans les concessions,
* la gestion des autorisations : concessionnaire et ayant droit,
* la gestion du terme de la concession : transfert de défunt, transfert à
  l'ossuaire,
* la gestion des concessions libres,
* la gestion des opérations funéraires,
* l'archivage systématique de l'ensemble des données pour constituer une mémoire
  commune.

Ce document a pour but de guider les utilisateurs et les développeurs dans la prise en main du projet. Bonne lecture et n’hésitez pas à venir discuter du projet avec la communauté à l’adresse suivante : https://communaute.openmairie.org/c/opencimetiere


Manuel de l'utilisateur
=======================

.. toctree::

   manuel_utilisateur/index.rst


Guide du développeur
====================

.. toctree::

   guide_developpeur/index.rst


Bibliographie
=============

* http://www.openmairie.org


Contributeurs
=============

(par ordre alphabétique)

* `atReal <http://www.atreal.fr>`_
* Florent Michon
* François Raynaud

